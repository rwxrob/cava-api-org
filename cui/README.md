# Conversational User Interface

A CAVA conversational user interface consists of three main components:

1. [Chat Box](./#chat-box)
2. [Screen](./#screen)
3. [Vocalizer](./#vocalizer)

## Chat Box

The *chat box* is where *voicable* message text is displayed and where the
user inputs messages, either by keyboard or voice.

It is paramount that the chat box fit on the device with enough room for the
keyboard to appear.

## Screen

The *screen* is where all *viewable* message content is sent. Triggered
events can also send content to the screen. The screen is secondary to the
chat box.

Usually the screen will display and take focus until it is dismissed when
the chat box will again take focus.

The user *must* be able to dismiss the screen without touching it when voice
is enabled.

## Vocalizer

The *vocalizer* is responsible for converting *voicable* message content
into speech utterances. It is also responsible to communicate the properties
of the utterance to anything that is rendering visual representations of the
waveforms and phonemes created by the utterance such as a lip-synch library
or robotic mouth when present.