# Welcome to CAVA!

![CAVA](./cava-api.png)
> A voice-first design for getting things done in the modern world

*You can pronounce it like the bubbly Spanish drink or the French "ça va".
We still haven't decided which one we like better either.*

CAVA is an open source, voice-first API design focused on creating a
conversational assistant and virtual agent for anyone who wants to build
one. Unlike many of the other frameworks out there, this effort aspires to
remain independent of any particular corporate influence and places priority
on the needs of the person for whom the assistant and agent are created.

## Version

v0.1.0

## The Basic Idea

The CAVA design provides one conversational (chat) assistant and one virtual
agent to everyone.

Why the two?

Think of how busy people get stuff done in a way that does not interfere with
their primary focus.

Such people often have both a personal assistant to attend to immediate
needs and an agent doing the time-consuming stuff that does not require the
person’s immediate interaction.

The assistant is responsive, light, fast and uses a Moleskin® or iPad to keep
track of stuff.

The agent is methodic, slower, persistent, quirky, thorough and prefers
spreadsheets or a database on a desktop computer.

Agents are really good at communicating with other agents

Assistants are really good communicating with the person they work for.

Often the agent and the assistant communicate with each other more than the
person might communicate to either of them directly. These two are the basis
of the CAVA model.