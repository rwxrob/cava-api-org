# Messaging Markup Language
> aka *CAVAML* or *MessagingML*

All CAVA messages must be written in *CAVA Messaging Markup* which has been
designed for anyone to easily learn in a few minutes.

> 💬 CAVA messaging markup is heavily influenced by Discord® and Medium®.

## Content Categories

Messages can be parsed into subdivisions of content that can be categorized
as *voicable*, *viewable*, and *external*.

### Voiceable

Voiceable content can be spoken by a standard speech synthesizer without difficulty.

### Viewable

Viewable content consists of images, videos, blocks of code and other
content that must be seen to be understood fully. 

Viewable content *must* be considered secondary to voiceable content when
creating CAVA messages. Some content can only been experienced through
sight but every effort must first be made to describe the viewable content
before sending it to the [screen](./screen/)

Such content should always
be fully described by other voiceable

### External

## Formatting

## Variables

## Triggers

## Emojis

## Links

## Fenced


## Examples

Here are some examples from which you can create your own message recipies.

### Basic State Variable

```md
Hi <name>, pleasure to meet you.
```

```md
 😁 <wave>
```

````
Have a look at this ![](screenshot.png) and tell me what you think.

You can have a single photo in a message.

![](photo.jpg)

And even something from a remote site. ![](https://some/mountain.jpg)

Perhaps you need an animated GIF to demo something. ![](switch.gif)

Or you can just [link](https://www.youtube.com/embed/6M78EWZlCvE) if easier.

The point is there is *very* little to remember.

Formatting is simplified to *em*, **strong**, and ***em-strong***.

Backticks provide `code` formatting or you can use three for preformatting

```js
console.log('useful for a lot of code')
// with multiple lines
```
````

### 207 Characters Maximum

Messages *must* be 207 characters or less (UNICODE code points, aka runes).
This is the average [maximum size a mobile device can
display](./assets/img/max-chars.png) while still displaying the keyboard.

## Triggers

Triggers are at the core of the *MessagingML* standard in CAVA. They can
be as simple as triggering the display of an image, pulling a stored
variable from the context state, or calling an *EventMessage* handler
that activates a robotic movement.

The use of triggers in this way is similar to how some computer games with
large amounts of voice also trigger body movements during the communication.

Messages containing triggers are split. The text up to the trigger becomes a
message, followed by the trigger identifier and so on.

1. If the trigger begins with a URI protocol render or fetch it
1. Check for a trigger event function handler and pass it the *MessageEvent*
   and replace the markup with the return value of the handler.
1. Check for any keys in the current context *State* and replace the markup
   with the corresponding value.
1. Replace the markup with an empty space.

[CLDR]: https://unicode.org/emoji/format.html#col-name

> 💬 Imagining the use of triggers with robots gets intesting when also
> considering the function of emojis in modern communication. Often an emoji
> will represent a body movement that accompanies the message. When deciding
> whether to use an emoji or a trigger always use the emoji first and assume
> any robotic renderer will understand the emoji. Use a trigger when more
> sophisticated handling is needed.

Trigger identifers follow the same standard as access CSS `style` properties
from within JavaScript. Dashes are removed and the letter immediately
following is capitalized.

```
last-name -> lastName
```

This conversion allows JavaScript simplified object key dotted notation to
be used when needed.

## HTML5 Rendering 

All messages are assumed to be converted to HTML5 before being passed to
whatever application is rendering them. This ensures maximum compatibility
and is therefore included in the standard.

*Note: This does not prevent other renderer implementations from parsing
MessageML directly. Transciption is not required by the standard. It simply
provides consistency for those that do use HTML.*
